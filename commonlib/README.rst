==============
 Horizon Core
==============
:Authors:
  * **A. Wilcox**
:Version:
  0.2
:Status:
  Alpha
:Copyright:
  © 2015-2017 Adélie Linux Team.  NCSA open source licence.


Introduction
============

This repository contains code and documentation for the core library of
Horizon, the next-generation installation system for the Adélie Linux
environment.  This library contains the common routines shared by the GUI
frontends, reducing code duplication and allowing new frontends to be written
quickly.

Note that Horizon Core is the *frontend* common library.  This may include
routines that read current configuration, disk layout, networking, packages
that can be installed, and so on.  This library is not responsible for the
actual installation of Adélie Linux to a computer.


License
```````

As the Adélie Linux project is an open-source Linux distribution, this package
is distributed under the same NCSA open source license as the distribution.


Changes
```````

Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Requirements
============

* CMake 3.4 or higher is required to build Horizon Core
* An implementation of udev (eudev, BusyBox, systemd, ...)
* pkgconf (pkg-config may work, but is deficient)

