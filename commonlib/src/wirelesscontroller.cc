/*
 * wirelesscontroller.cc - implementation of common WirelessController class
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2017 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#include <memory>               /* unique_ptr */

#include <horizon/wirelesscontroller.hh>

using Horizon::WirelessController;


WirelessController *WirelessController::wirelessController()
{
        static std::unique_ptr<WirelessController> oneTrueController(
                                new WirelessController);
        return oneTrueController.get();
}
