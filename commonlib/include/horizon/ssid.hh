/*
 * ssid.hh - miscellaneous 802.11 SSID structures
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2017 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#ifndef SSID_HH
#define SSID_HH

#include <string>

namespace Horizon
{


//! Represents an 802.11 wireless SSID.
struct SsidInfo
{
        //! The station's BSSID (typically a MAC address).
        std::string bssid;
        //! The frequency of the station (typically in MHz).
        int freq;
        //! The current strength of the station's signal.
        int signal_strength;
        //! Station flags including security type.
        int flags;
        //! The station's human-readable ESSID / name.
        std::string ssid;
        //! Whether the station requires a passphrase.
        bool passphrase;
        //! Whether the station uses WPA Enterprise.
        bool enterprise;
};


}

#endif /* !SSID_HH */
